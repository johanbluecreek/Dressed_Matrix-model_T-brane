# Dressed_Matrix-model_T-brane
Jupyter notebook performing some calculations of [1703.06106](https://arxiv.org/abs/1703.06106)

This notebook verifies in particular, Equation (2.5) and that it is solved by the solution displayed in Equation (3.6) and checks the expression found in Equation (3.7).

The calculations are carried out with the help of [SymPy](https://github.com/sympy/sympy).
